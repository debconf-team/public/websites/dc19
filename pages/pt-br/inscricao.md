---
name: Informações sobre a inscrição
---
# Informações sobre a inscrição

## Taxas de inscrição

Como sempre, a inscrição básica para a DebConf é de graça para os(as)
participantes.
Se você for participar do evento profissionalmente ou como um(a)
representante da sua empresa, nós pedimos para que você considere a
possibilidade de se inscrever em uma das nossas categorias pagas, para
ajudar a cobrir os custos da Conferência:

**Inscrição profissional**: com uma taxa de R$ 700,00 para a semana,
essa inscrição cobre os custos associados à sua participação
individual na Conferência.

**Inscrição corporativa**: com uma taxa de R$ 1.750,00, essa categoria
é feita para aqueles(a) que participarão da DebConf como
representantes de empresas, e ajuda a custear a Conferência para
outras pessoas.

Assim como nas três DebConfs anteriores, essas taxas de inscrição não
incluem alimentação ou hospedagem.

Nós encorajamos que todos(as) os(as) participantes ajudem a fazer da
DebConf um sucesso ao escolherem apropriadamente a categoria de
inscrição que mais se encaixa com seus perfis.

## Participação patrocinada

Qualquer membro da comunidade Debian pode solicitar bolsa (patrocínio)
para participar da DebConf.
Como em anos anteriores, nós faremos nosso melhor para oferecermos
auxílio com alimentação e hospedagem aos contribuidores Debian, mas a
oferta de patrocínio é sujeita à demanda e aos recursos financeiros
disponíveis. Decisões a respeito de solicitações de bolsa serão
divulgadas no final de abril de 2019.

## Como se inscrever?

Faça o cadastro no site, e após registre a sua inscrição no evento.
O link para inscrição está disponível na página inicial do site.
