---
name: Bolsas
---
# Candidatando-se para uma Bolsa para da DebConf

Há alguns tipos diferentes de bolsas disponíveis para a DebConf,
patrocinadas através de diferentes orçamentos:

* Bolsas patrocinadas pelo Debian estão disponíveis para
  contribuidores(as) ativos do Debian.
* Bolsas de diversidade do Debian estão disponíveis para novos(as)
  participantes do Debian/DebConf, especialmente para comunidades
  sub-representadas.

## Bolsas patrocinadas pelo Debian

Qualquer membro da comunidade Debian pode solicitar uma bolsa para
participar da DebConf. Como ocorrido em anos anteriores, nós vamos
fazer o nosso melhor para oferecermos alimentação e hospedagem
patrocinadas para os(as) contribuidores(as) Debian, mas a
possibilidade de ajuda está sujeita à demanda e aos recursos
financeiros disponíveis.

Nas instruções seguintes, "nos dizer" significa preencher o campo
"Detalhes da minha solicitação de bolsa" no formulário de inscrição.

## Se você está se candidatando a uma bolsa de hospedagem ou alimentação

Se você está solicitando uma bolsa para hospedagem, você ficará hospedado(a)
no hotel reservado para a Conferência, e dividirá o quarto com outras pessoas.
O café da manhã está incluso.

Se você está solicitando uma bolsa para alimentação, você terá o almoço
e janta no restaurante reservado para a Conferência.

Nós iremos olhar
o [contributors.debian.org](https://contributors.debian.org) para
termos uma ideia geral da sua atividade nos últimos anos.  Se você
está satisfeito(a) com o panorama que o site dá, você não precisa nos
dizer mais nada.  Se você quiser, pode nos dizer mais a respeito da
sua atividade recente no Debian, ou sobre seus planos para o futuro
próximo.

Se você está se candidatando a uma bolsa de hospedagem ou alimentação
para a DebCamp, tenha certeza de nos dizer (no campo "Detalhes da
minha solicitação de bolsa") sobre seus planos para o evento. O
objetivo da DebCamp é realizar trabalho focado no Debian; por favor,
enfatize de que modo (por exemplo, oportunidades de colaboração e/ou
de ficar sozinho) participar da DebCamp vai ajudá-lo(a) a completar
tarefas que, de outro modo, você não conseguiria.

## Se você está se candidatando a uma bolsa de viagem

Nós precisamos decidir como gastar uma quantidade limitada de dinheiro
de modo a maximizar os benefícios para o projeto Debian. Isso não deve
desencorajá-lo(a) de candidatar-se, mas por favor nos dê as
informações que necessitamos para podermos tomar boas decisões.

* Nos diga o que você considera serem suas contribuições mais
  importantes ou interessantes para o Debian nos últimos dois
  anos. Nós vamos olhar
  em [contributors.debian.org](https://contributors.debian.org), mas
  muitas às vezes os detalhes não estão lá.
* Nos diga quais são seus planos para o próximo ano, e como participar
  da DebConf vai ajudá-lo(a) com eles.
* Estime honestamente o seu nível de necessidade de ajuda
  financeira. Isso não é fácil, mas tem um papel importante durante
  nossa tomada de decisão.
* Nos dê uma estimativa dos seus gastos da viagem. Isso também será o
  patrocínio máximo que você irá solicitar. Em geral, essa quantia
  deve refletir o meio mais econômico de viagem até o local do
  evento. Se a quantia for maior por alguma razão, nos diga. Se for
  menor porque você (ou outra pessoa) irá pagar uma parte, nos diga
  também.
* Se existe alguém que conhece bem suas contribuições no Debian, e
  esteja disposto(a) a recomendar você, nos diga seu nome e peça a
  ele(a) que nos escreva diretamente
  para [bursaries@debconf.org](mailto:bursaries@debconf.org).
* Se sua contribuição principal ao Debian é ajudar com a DebConf, por
  favor peça para que alguém do time com o qual você está envolvido
  entre em contato conosco e nos diga o quanto eles precisam de você
  na edição desse ano da DebConf.

Todos(as) aqueles(as) que forem selecionados(as) para as bolsas de
viagem também poderão receber patrocínio para hospedagem e/ou
alimentação caso solicitem.
Note que se você nos disser "Não posso participar de maneira alguma"
sem uma bolsa de viagem, nós assumiremos que você precisará de
hospedagem e/ou alimentação somente se a bolsa de viagem for aprovada.

## Bolsas de Diversidade

O objetivo dos Patrocínios de Diversidade é trazer novas pessoas à
DebConf — pessoas que estão começando suas jornadas pelo Software
Livre ou com o Debian, contribuidores(as) que estão realizando
contribuições que não envolvam código-fonte, e contribuidores(as) que
estão tentando vir à sua primeira DebConf. Se você está interessado(a)
em se candidatar para uma bolsa de diversidade, por favor nos conte um
pouco sobre você e sobre o motivo de você querer vir para a DebConf:

* Você acha que essa é a chance de você realizar sua primeira
  contribuição?
* Você usa Debian e quer aprender mais sobre o projeto e sobre como
  participar dele?
* Você gosta muito de Software Livre, e está animado(a) para fazer
  parte da comunidade Debian?

Nós especialmente gostaríamos de receber pedidos de:

* Mulheres (cic, trans, queer), indivíduos não-binário e genderqueer,
  e homens trans.
* Residentes de países africanos, países fora da Austrália, América do
  Norte e Europa Ocidental, e pessoas de minorias raciais/étnicas nos
  seus países de residência.
* Indivíduos acima de 35 anos.

Patrocínios de diversidade completos oferecerão aproximadamente € 2
000 (EUR) / $ 2 270 (USD).
Hospedagem, alimentação e viagem serão pagas com esse dinheiro.

## Reembolso da bolsa de viagem

Em geral apenas as despesas com *transportes de longas distâncias*
são reembolsadas, como passagens de avião/barco/ônibus para chegar em
Curitiba e de Curitiba de volta para casa. Não está incluso outros gastos
eventuais como visto, refeições e hospedagem durante a viagem.

As bolsas de viagens são reembolsadas após a DebConf. O processo é:

1. Compre as suas passagens.
1. Guarde os recibos das compras das passagens que você pedirá o reembolso.
1. Depois de participar da DebConf, envie o pedido de reembolso usando o
   [formulário de reembolso da SPI][] para solicitar o recebimento do valor
   gasto.

[formulário de reembolso da SPI]: https://spi-inc.org/treasurer/reimbursement-form

Os reembolsos das despesas relacionadas a DebConf serão feitas pela instituição
estrangeira "Software in the Public Interest" (SPI) para todos os solicitantes,
incluindo brasileiros e europeus.

Se você tiver qualquer dúvida sobre o processo de reembolso, ou sobre o que é
possível solicitar reembolso, por favor envie um e-mail em inglês para
bursaries@debconf.org assim que for possível.

### Requisitos específicos por país

**Brasil**: Para o recebimento do reembolso usando bancos brasileiros, você
deverá adicionar os números do seu CPF e do seu telefone no corpo da mensagem
no "Step 5" das orientações. A SPI precisará dessas informações adicionais
para transferir o dinheiro.
