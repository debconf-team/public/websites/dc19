---
name: Financiamento Coletivo
---
# Debian Brasil - Financiamento Coletivo

A Comunidade Debian Brasil realizou um [projeto de financiamento coletivo](https://www.catarse.me/debconf19) com o intuito de demonstrar o engajamento da comunidade local para a realização da DebConf19 - Conferência mundial de desenvolvedores(as) do Projeto Debian. A meta era tornar a comunidade brasileira um dos patrocinadores da DebConf19, e conseguimos o valor de R$5.930,00 suficiente para tornar a Comunidade Debian Brasil um "Supporter" da conferência.

Agradecemos a contribuição de todos(as) que nos ajudaram nesse projeto:

* Adriana Cassia da Costa
* Áleson Medeiros (@pinguimcriativo)
* Alex Pelegrini
* Alexandre
* Alisson Coelho de Morais
* André Marcelo Alvarenga
* Antonio C C Marques
* Antonio Carlos Ribeiro Nogueira
* Arthur de Moura Del Esposte
* Ayrton Morcelli Junior
* Barbara Samel Rocha Tostes
* Carlos Donizete Froes
* César Túlio Albuquerque de Almeida
* Daniel Lenharo
* Daniel Pimentel
* Douglas Lara
* Elías Alejandro Año Mendoza
* Emerson Sachio Saito
* Eriberto Mota
* Fabio Tobich
* Felipe Augusto Nunes Ribeiro
* Felipe Do E. Santo
* Fernando Ike de Oliveira
* Fernao Vellozo
* Flavio Casas de Arcega
* Francisco Aparecido da Silva
* Gabriel F. T. Gomes
* Giovani Ferreira
* Gustavo Theodoro Laskoski
* Helio Loureiro
* Henrique de Moraes Holschuh
* Jeann Wilson Aguiar Cavalcante
* Jonathan Bustillos Osornio
* Jorge Peixoto de Morais Neto
* Jorjeh Penedo
* Juliana Tibães
* Leandro Ramos
* Leonardo Fiedler
* Leonardo Rocha
* Lucas Kanashiro
* Luciano Antonio da Silva
* Marcio de Souza Oliveira
* Maurício Meneghini Fauth
* nathalia moreira
* Paulo Henrique de Lima Santana
* Paulo Roberto Alves de Oliveira
* Pedro Arthur P. R. Duarte
* Pedro Cassone
* Ramiro Batista da Luz
* Ricardo Fontoura
* Samuel Henrique (samueloph)
* Tania Silva
* Thiago Andrade Marques
* Thiago Andrade Nunes
* Tiago Rocha
* Tobias Sette Ferreira
* VALESSIO SOARES DE BRITO
* Wellton Costa de Oliveira
* wendell da silva

