---
name: Contact us
---
# Contact us

## DebConf Team

If you want to volunteer for DebConf, you can contact the DebConf Team
mailing list: [debconf-team@lists.debian.org][].
This list is used for general organization.
It has open subscription and [public archives][].

For live discussions, you can connect to the DebConf IRC channel (chat)
with the client of your choice: [#debconf on OFTC.net][#debconf] or use
your browser with a [webchat][webchat-#debconf].

## Accommodation and Registration

All questions and inquiries regarding either accommodation or
registration should be directed to the Registration Team:
[registration@debconf.org][]

You can send your message in portuguese or in english.

## By Phone

If you run into any problems, you can call the DebConf Front Desk at
[+55 41 99791-3565](tel:+5541997913565).

## Talks

Questions and inquiries about talks and paper submissions:
[content@debconf.org][]

## Sponsorship (fundraising)

Send your questions and inquiries about sponsorship and becoming a
sponsor: [sponsors@debconf.org][].

## Visa

Legal responsibilities regarding visa, conference invitations fall on
our Visa Team [visa@debconf.org][].

## Harassment and inappropriate behaviour

Please see the [Code of Conduct](../about/coc/) page for more information.

[debconf-team@lists.debian.org]: mailto:debconf-team@lists.debian.org
[public archives]: https://lists.debian.org/debconf-team/
[registration@debconf.org]: mailto:registration@debconf.org
[sponsors@debconf.org]: mailto:sponsors@debconf.org
[content@debconf.org]: mailto:content@debconf.org
[visa@debconf.org]: mailto:visa@debconf.org
[#debconf]: irc://irc.debian.org/debconf
[webchat-#debconf]: https://webchat.oftc.net/?nick=&channels=#debconf
