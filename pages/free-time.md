---
name: Free Time on 28th
---
# Free time on 28th

On 28th morning you can visit some places before you go to our brazilian barbecue. You have some suggetions:

## ***Feira do Largo da Ordem*** (Largo da Ordem Market Fair)

It's a big and very nice handcraft fair.

[Vídeo](https://www.youtube.com/watch?v=0B_b1TTJLyw)

You can go walking from the Hotel Nacional Inn

<i class="fa fa-map" aria-hidden="true"></i>
[Map](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=-25.4374%2C-49.2680%3B-25.4275%2C-49.2739#map=15/-25.4324/-49.2707)

## Jardim Botânico

Take the public bus in front of the hotel and 10 minutes after you will arrive at [Jardim Botânico](http://www.turismo.curitiba.pr.gov.br/fotos/jardim-botanico/9/100/)

<i class="fa fa-map" aria-hidden="true"></i>
[Map](https://www.google.com/maps/dir/Hotel+Nacional+Inn+Curitiba+-+R.+Louren%C3%A7o+Pinto,+458+-+Centro,+Curitiba+-+PR,+80010-160/Jardim+Bot%C3%A2nico+de+Curitiba+-+Rua+Engenheiro+Ostoja+Roguski+-+Jardim+Bot%C3%A2nico,+Curitiba+-+PR/@-25.4387777,-49.2622553,15z/data=!4m15!4m14!1m5!1m1!1s0x94dce468e7c0093b:0x714515c465375b18!2m2!1d-49.2678038!2d-25.4373688!1m5!1m1!1s0x94dce500d5e96635:0x1a2d5ec5fddc5654!2m2!1d-49.2387704!2d-25.4420753!3e3!5i2)
