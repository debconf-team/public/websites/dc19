---
name: Dinner
---

# Lunch and Dinner

Lunch and dinner will be served in two spaces, depending of the day:

* Restaurant at venue. It's called **RU** (Restaurante Unviersitário).
* Restaurant at hotel.

The schedule is:

| **MEALS**                   |           |
|-----------------------------|-----------|
| 14 (Sunday) - Lunch         | Hotel     |
| 14 (Sunday) - Dinner        | Hotel     |
| 15 (Monday) - Lunch         | RU        |
| 15 (Monday) - Dinne         | RU        |
| 16 (Tuesday) - Lunch        | RU        |
| 16 (Tuesday) - Dinner       | RU        |
| 17 (Wednesday) - Lunch      | RU        |
| 17 (Wednesday) - Dinner     | RU        |
| 18 (Thursday) - Lunch       | RU        |
| 18 (Thursday) - Dinner      | RU        |
| 19 (Friday) - Lunch         | RU        |
| 19 (Friday) - Dinner        | RU        |
| 20 (Saturday) - Lunch       | RU        |
| 20 (Saturday) - Dinner      | Hotel     |
| 21 (Sunday) - Lunch         | Hotel     |
| 21 (Sunday) - Dinner        | Hotel     |
| 22 (Monday) - Lunch         | RU        |
| 22 (Monday) - Dinne         | RU        |
| 23 (Tuesday) - Lunch        | RU        |
| 23 (Tuesday) - Dinner       | RU        |
| 24 (Wednesday) - Lunch      | RU        |
| 24 (Wednesday) - Dinner     | RU        |
| 25 (Thursday) - Lunch       | RU        |
| 25 (Thursday) - Conf Dinner | Concórdia |
| 26 (Friday) - Lunch         | RU        |
| 26 (Friday) - Dinner        | RU        |
| 27 (Saturday) - Lunch       | Hotel     |
| 27 (Saturday) - Dinner      | Hotel     |
| 28 (Sunday) - Barbecue      | APUFPR    |
| 28 (Sunday) - Dinner        | Hotel     |

More information:
* [Menu](https://wiki.debian.org/DebConf/19/CateringFood#Menu)
* [Conference Dinner](/about/confdinner/)
* [Barbecue](/lastday-brazilian-barbecue/)
