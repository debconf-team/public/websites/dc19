---
name: Call for Proposals
---

# Call for Proposals

The DebConf Content team would like to call for proposals in the DebConf 19
conference, which will take place in Curitiba, Brazil, between July 21st and
28th. It will be preceded by DebCamp from July 14th to 19th, and Open Day on
the 20th.

You can find this Call for Proposals, in its latest form, online:

  https://debconf19.debconf.org/cfp/

Please refer to this URL for updates on the present information.


## Submitting an Event

You can now submit an [event proposal]. Events are not limited to traditional
presentations or informal sessions (BoFs): we welcome submissions of tutorials,
performances, art installations, debates, or any other format of event that you
think would be of interest to the Debian community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks, ...)
could have different durations. Please choose the most suitable duration for
your event and explain any special requests.

You will need to create an account on the site, to submit a talk. We suggest
that Debian account holders (including DDs and DMs) to use [Debian SSO] when
creating an account. However, this isn't required, as you can sign up with an
e-mail address and password.

[Debian SSO]: https://wiki.debian.org/DebianSingleSignOn
[event proposal]: https://debconf19.debconf.org/talks/new/


## Timeline

If you depend on having your proposal accepted in order to attend the
conference, please submit it in a timely fashion so that it can be considered
(and potentially accepted) as soon as possible.

All proposals must be submitted before Sunday May 12th, 2019 to be evaluated
for the official schedule.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have some
broad topics on which we encourage people to submit proposals, including but not
limited to:

- Cloud and containers
- Debian Blends
- Debian in Science
- Embedded
- Introduction to Free Software & Debian
- Packaging, policy, and Debian infrastructure
- Security
- Social context
- Systems administration, automation and orchestration

You are welcome to either suggest more tracks, or to become a coordinator for
any of them. For more information, see the [Content team wiki].

[Content team wiki]: https://wiki.debian.org/DebConf/19/Content

## Open Day

This call for proposals also targets [Open Day], a day of activities targeted
at the general public on July 20th. Topics of interest range from topics
specific to Debian to the greater Free Software community and maker movement.
The idea of Open Day is to bring the general public closer to Debian and
vice-versa, so activity proposals that go in that direction are more than
welcome.

[Open Day]: https://debconf19.debconf.org/schedule/openday/

If you are interested in presenting on Open Day, let us know in the "Notes"
field of your submission. We might also invite proponents that are not
specifically targeting Open Day to present in it if we find that the topic
fits the above goals.

The Open Day will host activities in multiple languages.
We expect to have activities in English, Portuguese, and Spanish.

If your talk will be in portuguese, you can write the Abstract field
in portuguese too.

## Talk proposal help on IRC

This year we will be holding holding office hours on IRC. Those will be
designated times where the DebConf content team will be available to help
potential speakers prepare their talk proposals for DebConf.

Dates and times for those will be announced later.


## Code of Conduct

Our event is covered by a [Code of Conduct] designed to ensure everyone’s safety
and comfort. The code applies to all attendees, including speakers and the
content of their presentations. Do not hesitate to contact us at
<content@debconf.org> if you have any questions or are unsure about certain
content you’d like to present.

[Code of Conduct]: https://debconf.org/codeofconduct.shtml


## Video Coverage

Providing video is one of the [conference goals], as it makes the content
accessible to a wider audience. Unless speakers opt-out, scheduled talks may be
streamed live over the Internet to promote remote participation, and recordings
will be published later under the [DebConf license] (MIT/Expat), as well as
presentation slides and papers whenever available.

[conference goals]: https://debconf.org/goals.shtml
[DebConf license]:  https://meetings-archive.debian.net/pub/debian-meetings/LICENSE


## Closing note

DebConf 19 is still accepting sponsors; if you are interested, or think you
know of others who would be willing to help, please get in touch with
<sponsors@debconf.org>.

In case of any questions, or if you wanted to bounce some ideas off us first,
please do not hesitate to reach out to the content team at <content@debconf.org>.


We hope to see you in Curitiba!

The DebConf team
