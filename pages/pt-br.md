---
name: DebConf19
---

# Sobre a DebConf

A **DebConf é a Conferência mundial de desenvolvedores(as) do Projeto Debian**.
O evento é realizado anualmente e é itinerante, acontecendo em uma cidade/país
diferente a cada ano.

**A DebConf é um evento aberto a todos(as), independente do seu nível de
conhecimento sobre Debian.** Queremos receber desde usuários(as) inexperientes que
estão iniciando o seu contato com o Debian até Desenvolvedores(as) oficiais do
Projeto. Ou seja, estão todos(as) convidados(as)!

Além de uma programação completa com palestras técnicas, sociais e políticas, a
DebConf cria uma oportunidade para desenvolvedores(as), contribuidores(as) e
outras pessoas interessadas se conhecerem presencialmente e trabalharem juntas
de maneira mais próxima.

A [DebConf19](..) será a 20ª edição da Conferência e acontecerá de 21 a 28 de
julho de 2019, no Campus Central da
[UTFPR - Universidade Tecnológica Federal do Paraná](http://utfpr.edu.br), em
Curitiba - Paraná.

Na semana anterior à DebConf, de 14 a 19 de julho, acontecerá a **DebCamp**, que é
um período em que participantes do Projeto se reunirão para
trabalharem nas melhorias do sistema operacional. E no dia 20
de julho acontecerá o [Open Day](/pt-br/openday/), um dia totalmente
dedicado ao público local com atividades que vão além do Debian, contendo
temas relacionados a Software Livre e Código Aberto em geral.

A Conferência é uma ótima oportunidade para usuários(as) interessados(as) em
colaborar para [melhorar o Debian](https://www.debian.org/intro/help).
[Conferências anteriores do Debian](https://www.debconf.org) contaram com
palestrantes de destaque de todo o mundo.
A [DebConf18](https://debconf18.debconf.org) aconteceu em Hsinchu, Taiwan, e
teve a participação de 298 pessoas de 44 países. Esta será a segunda vez que o
Brasil sediará uma DebConf. A primeira vez foi a
[DebConf4](https://debconf4.debconf.org) em 2004 em Porto Alegre.

## Sobre o Debian

O [Projeto Debian](http://debian.org) é uma associação de indivíduos que têm
como causa comum criar um sistema operacional livre. O sistema operacional que
criamos é chamado Debian.

Um sistema operacional é o conjunto de programas básicos e utilitários que
fazem seu computador funcionar. No núcleo do sistema operacional está o kernel.
O kernel é o programa mais fundamental no computador e faz todas as operações
mais básicas, permitindo que você execute outros programas.

Os sistemas Debian atualmente usam o kernel Linux ou o kernel FreeBSD. O Linux
é uma peça de software criada inicialmente por Linus Torvalds com a ajuda de
milhares de programadores espalhados por todo o mundo. O FreeBSD é um sistema
operacional incluíndo um kernel e outros softwares.

O Debian vem com mais de 50.000 pacotes (softwares pré-compilados e empacotados
em um formato amigável, o que faz com que sejam de fácil instalação em sua
máquina), um gerenciador de pacotes (APT) e outros utilitários que tornam
possível gerenciar milhares de pacotes em milhares de computadores de maneira
tão fácil quanto instalar um único aplicativo. Todos eles são livres.

Saiba mais no [website do Debian](https://www.debian.org/).

## Como ajudar na organização

A DebConf é organizada por uma equipe internacional que contém pessoas do
Brasil e de outros países, e pode ser [contatada em inglês de diversas formas](/contact/).

Se você quer ajudar a organizar a DebConf19 e não se sente confortável para se
comunicar em inglês, veja a página sobre [como ajudar em português](help/).
