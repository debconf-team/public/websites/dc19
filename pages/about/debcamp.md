---
name: DebCamp
---
# DebCamp

You can read more about DebCamp here:

[https://wiki.debian.org/DebConf/19/DebCamp](https://wiki.debian.org/DebConf/19/DebCamp)
