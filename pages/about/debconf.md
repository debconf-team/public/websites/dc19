---
name: About DebConf
---
# About DebConf

[Em português](/pt-br/)

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. [DebConf18][] took place in Hsinchu,
Taiwan and was attended by 298 participants from 44 countries.

[DebConf18]: https://debconf18.debconf.org/

**DebConf19 is taking place in Curitiba, Brazil from July 21st to 28th, 2019.**

It is being preceded by DebCamp (July 14th to 19th), and
[Open Day](/schedule/openday/) (July 20th).

We look forward to seeing you in Curitiba!

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

## The Venue

The [Federal University of Technology — Paraná (Universidade Tecnológica Federal do Paraná — UTFPR)][utfpr],
is a historic building located in central Curitiba.
The [venue](../venue/) offers several conference halls and classrooms for
plenaries, presentations, discussions, and informal workshops.
[Accommodation](../accommodation/) for participants is available in
hotels nearby.

[utfpr]: http://portal.utfpr.edu.br/english

## Codes of Conduct and Anti-harassment

Please see the [Code of Conduct](../coc/) page for more information.
