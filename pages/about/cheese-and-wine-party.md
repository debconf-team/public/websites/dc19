---
name: Cheese and Wine Party
---
# Cheese and Wine Party

<a href="https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/CuritibaMunicipalMarketCheeses"
   title="Did you say cheese??">
   <img class="img-fluid" src="{% static "img/cheeses.jpg" %}">
</a>

Cheese is good for you. Wine also helps.
This will be the 14th official DebConf Cheese and Wine party.
The 0th party was improvised in Helsinki during DebConf 5, in the
so-called "French" room.
It then found its way into the official timetable at DebConf 6, in
Mexico, and has been a DebConf tradition ever since.

The concept is very simple: bring tasty things from your country,
preferably edible.
Grapes and lactose are both optional: we like cheese and wine, but we
love the surprising things that people bring from all around the world.
Even if you do not bring anything, feel free to participate: our
priorities are our users and free cheese.

Some bits from previous editions:

 * [C&W13, DebConf17, Montreal, Canada, August 7th 2017](https://www.flickr.com/photos/aigarius/36398984276/in/photostream/)
 * [C&W14, DebConf18, Hsinchi, Taiwan, August 30th  2018](https://photos.google.com/share/AF1QipOroi0hV38ezF2Oa_5CmtpIvdam2rLJ2Dy6oIyzGD6AJ_Emfrrh573YxmyVoS-vUQ/photo/AF1QipNQYvb8ViztpXfkg3neI4eV0HsMC5Yt7GSayFA7?key=UEZ5VVphTlJhS2xTTl9vVjdxM0ZmbWlFcC1rTHBB)

## When?

 * July 22nd, 2019 (Monday).
 * 7:30 PM

## Where?

Main hall at Universidade Federal do Paraná (UFPR) - Campus Rebuças.
The entrance is in front of Estação Mall.

 * [Route from Venue - 440m](https://www.openstreetmap.org/directions?engine=fossgis_osrm_foot&route=-25.43858%2C-49.26868%3B-25.43747%2C-49.26483#map=18/-25.43805/-49.26695&layers=N)
 * [Route from Hotel - 340m](https://www.openstreetmap.org/directions?engine=fossgis_osrm_foot&route=-25.43731%2C-49.26760%3B-25.43747%2C-49.26483)

## FAQ

Can I participate in the party (i.e., eat and drink) if I haven't brought
anything?

 * Sure, you can. We do not require you to bring anything to share.
 * However, the greater the cheesy variety, the better the party will be.
 * If it is not possible to bring cheese or wine, you can buy some locally in
Curitiba (see below), or you can bring some other food or drink typical of your
region.

I couldn't bring anything from home but would like to participate. Is there a way to do so?

 * You can buy something at [Curitiba Municipal Market](http://mercadomunicipaldecuritiba.com.br)
or supermarkets near from the venue.
 * You can help us too! C&W helpers meetings will be announced on [debconf-announce](https://lists.debian.org/debconf-announce/).

I brought XXX and do not know where to store it!

 * There will be a fridge on-site.

I forgot what I brought!

 * Please make sure your C&W submissions are listed in the wiki, and labelled, so that this does not happen.

Is there anything special I should do when bringing my C&W submissions?

 * Please make sure all items are clearly identified. If the packaging does not
say what you've brought, please label it yourself. The label should say what it
is, where it comes from, and (if relevant) what it is made of.
 * If your item unexpectedly contains allergens or things people may wish to
avoid (e.g. shellfish, pork, alcohol), please point it out.

Vegans do not eat cheese. Can they still attend?

 * Yes! We accept (and encourage) the submission of vegan friendly food
(including vegan cheese).

I do not drink alcohol. Can I still attend?

 * Yes! We will have water, sodas and juices too.

Where can I buy Brazilian, or other foreign cheeses, in Curitiba?"

 * [Here's an article on local cheese shops](http://www.gazetadopovo.com.br/bomgourmet/onde-comprar-queijos-brasileiros-e-internacionais-em-curitiba)

## Brazil customs regulations

You can import up to 5 kilograms of cheese per person.

 * <http://www.gazetadopovo.com.br/viver-bem/turismo/que-delicias-estrangeiras-voce-pode-trazer-na-bagagem>
 * <http://g1.globo.com/economia/noticia/2016/05/turistas-agora-podem-entrar-no-pais-com-queijo-salame-e-doce-de-leite.html>

You may bring up to 10 kg (in total) of meat products, e.g. salami, sausage, ham, paté.

Total milk and milk products, such as cheeses and yoghurts, must not exceed 5 kg or 5 liters.

There is a large alcohol allowance — you may bring up to 12 litres (or 500 USD)
of beer and wine, per passenger.

 * <http://www.agricultura.gov.br/assuntos/vigilancia-agropecuaria/produtos-de-origem-animal-autorizados-em-bagagem>
 * <http://www.agricultura.gov.br/noticias/trabalho-de-caes-farejadores-do-vigiagro-tem-87-de-eficiencia/in-11.pdf>

## Would you like to help us?

There will be a volunteer sign-up during the conference.

We will need volunteers to receive and store cheese donations from
attendees, during DebCamp and DebConf.

And on the day of the party, we will need a team to prepare venue and
cheese, and clean up afterwards.
